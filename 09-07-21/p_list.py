colors = ["red","green","blue","magenta","black"]
print(colors)

#Indexing
print(colors[0])
print(colors[-1])

#Slicing
print(colors[1:])
print(colors[:1])
print(colors[-2:])

#Stepping
print(colors[::1])
print(colors[::2])
print(colors[::-1])