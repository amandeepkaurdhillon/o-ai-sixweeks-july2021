games = ("PubG","Candy Crush","Clash of Clanes")

print(games)
print(type(games))

print(games[1])
print(games[-1])
print(games[1:])
print(games[:1])
print(games[::2])
print(games[::-1])

print("PubG" in games)
print("PubG" not in games)

# games[0]="COD"
# del games[0] ## Tuple is immutable ; doesn't support edit,update operations
for i in games:
    print(i)

# print(dir(games))

a = (1,2,4,5,6,33,3,20,20,20)
b = (100,200)

print(a.index(2))
print(a.count(20))

print(a+b)
print(b*2)

a = list(a)
a.sort()
print(a)
print(tuple(a))