import random 

print("Enter 'exit' to exit !")
score = 0
while True:
    n = random.randint(1,10)
    print("generated:",n) 
    name = input("Enter Number to guess: ") 
    if name=="exit":
        break
    elif name=="score":
        print("Your Score: ",score)
    
    elif name.isdigit():
        if int(name)==n:
            score+=1
            print("Congratulations! You cracked!!!")
            # print("Your Score: {}".format(score))
        else:
            if score>0:
                score-=1
            print("OOPs! Incorrect Guess! Try Again!!!")
            # print("Your Score: ",score)
    else:
        print("Invalid Number!\n")
    
    # print("Welcome {}!\n".format(name))