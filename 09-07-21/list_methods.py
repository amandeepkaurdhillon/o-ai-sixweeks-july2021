weekdays = ["Monday","Tuesday","Wednesday","Thursday","Friday"]
offdays = ["Saturday", "Sunday"]

#Concatenation
print(weekdays+offdays)

#Iteration
print(offdays*2)

#Membership
print("Saturday" in weekdays)
print("Saturday" not in weekdays)