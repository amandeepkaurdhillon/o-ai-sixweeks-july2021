lang = ["Python","PHP","Ruby","React","Angular","ANGULAR",'234','54','3']

print("Before: {} Total: {}".format( lang, len(lang)))
## Insert Element
# lang.append("Java")
# lang.insert(1,"Flutter")

## Edit/Update Elements
# lang[3]="HTML"

## Delete Elements
# lang.pop()
# lang.pop(1)
# lang.remove("Ruby")
# print(dir([]))

# c = lang.copy()
# lang.clear()
# print(lang.index("Ruby"))
lang.sort()
print("\nAfter: {} Total: {}".format( lang, len(lang)))
# print("C=",c)

a = [34,655,2,6,134,4]
# a.sort()
a.reverse()
print(a)


x = [100,200,300]
y = [1,2,3]

# x.append(y)
x.extend(y)
print(x[5])
# print(x[3][2])

name = input("Enter Language to search: ")
if name in lang:
    print("{} found at {}".format(name, lang.index(name)))
else:
    print("{} not found in language list!".format(name))
