file = open("table.html","a")
print("Ponter: ", file.tell())
n = int(input("Enter Number: "))
st="<link rel='stylesheet' href='style.css'>"
st +="<h1 style='color:white;background:black;'>Here is the table of : {}</h1>".format(n)
for i in range(1,11):
    st+='<h1 align="center" style="color:red">{} x {} = <span style="color:green;">{}</span></h1>'.format(n,i,n*i)
st+="<br/>"

file.write(st)
print("Ponter: ", file.tell())
file.close()
print("File written successfully!")