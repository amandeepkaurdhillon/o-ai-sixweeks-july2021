
import requests

URL = "https://restcountries.eu/rest/v2/all"
data  = requests.get(URL).json()
name = input("Enter Country Name: ")

flag = False
for con in data:
    if name.lower() == con["name"].lower():
        flag = True
        print("\t Name: {}".format(con["name"]))
        print("\t Capital: {}".format(con["capital"]))
        print("\t Area: {}".format(con["area"]))
        print("\t Population: {}".format(con["population"]))
        print("\t Flag: {}".format(con["flag"]))
        print("\t Borders:")
        for c,b in enumerate(con["borders"], start=1):
            print("\t\t{}. {}".format(c,b))
        print("\t Languages:")
        for count,lang in enumerate(con["languages"], start=1):

            print("\t\t{}. {}".format(count, lang["name"]))
if flag==False:
    print("'{}' is not a valid country name".format(name))