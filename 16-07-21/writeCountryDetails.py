import requests
name = input("Enter Country Name: ").title()
path = "countries/{}.html".format(name)

try:
    f = open(path, 'x')
    #Write API data to a file 
    URL = "https://restcountries.eu/rest/v2/all"
    data  = requests.get(URL).json()
    
    st =""
    for i in data:
        if i["name"].title()==name:
            st+="<div style='margin:50px 20%;float:left;width:25%;box-shadow:0px 0px 10px black;padding:20px;'>"
            st+="<img src='{}' style='height:250px;width:100%'>".format(i["flag"])
            st+="<h1>{}</h1>".format(i["name"])
            st+="<p><em>Population:{}</em></p>".format(i["population"])
            st+="<p>Borders: {}</p>".format(i["borders"])
            st+="</div>"
            f.write(st)
            f.close()
            print('{} created successfully!'.format(name))
except FileExistsError:
    print("A file with this name already exists!")
    