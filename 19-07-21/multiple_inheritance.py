clz ="ABCD GROUP OF COLLEGES"
class Student:
    clz = "XYZ College of Science"
    name = input("Enter Student Name: ")
    def intro1(self):
        print("This is a member function of Student Class")

class Library:
    books = ["AI", "Mathematics","Physics"]
    def book_details(self):
        print("There are total 100000 books in library")

#Multiple Inheritance
class Account(Student, Library):
    clz = "DEMO COLLEGE"
    def fee_details(self):
        print("College : {}".format(self.clz))
        self.intro1()
        print("Fee details of: {}".format(self.name))
        print("Famous Books: {}".format(self.books))
        self.book_details()

obj = Account()
obj.fee_details()
