class Employee:
    company_name = "ABCD Tech"
    def __init__(self,name="Mr. Payen", salary=10000):
        self.n = name 
        self.s = salary

class Account(Employee):
    def total_salary(self, overtime):
        print("Salary Details of: {}".format(self.n))
        print("Total Salary: Rs.{}/-".format(self.s+overtime))

class Performance(Employee):
    def get_performance(self, AP, CP):
        print("Performance stats of {}".format(self.n))
        print("He has received {} projects".format(AP))
        print("And completed {} projects so far".format(CP))
        print("Pending: {}".format(AP-CP))

o = Performance()
o.get_performance(10,3)

a = Account()
a.total_salary(3000)