class GrandParent: # a, info1()
    a = "hello"
    def info1(self):
        print("MF Grand Parent")

class Parent(GrandParent): #a,b, info1(), info2()
    b = "world"
    def info2(self):
        print("MF Parent")

class Child(Parent): # a,b,c, info1(), info2(), info3()
    c = "there"
    def info3(self):
        print("MF Child Class")

obj = Child()
print(obj.a)
print(obj.b)
print(obj.c)
obj.info1()
obj.info2()
obj.info3()

myobj = Parent()
print(myobj.a , myobj.b)

print(issubclass(Parent, GrandParent))
print(issubclass(Parent, Child))
