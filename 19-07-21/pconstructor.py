class Employee:
    def __init__(self, name, salary, age):
        self.n = name 
        self.s = salary 
        self.a = age
        # return self.n ##It will return error as  __init__() should return None
    def get_info(self):
        print("{} is {} years old and earns Rs.{}/-".format(
            self.n, self.a, self.s
        ))
    def __del__(self):
        del self.n
        del self.a
        print("Destructor called!")
        # print(self.n) ##it will return error as parameter has been deleted

#Create object and initialize value
e1 = Employee("Peter Parker",100000, 22)
e2 = Employee("James Parker",10000, 25)
e3 = Employee("Rohn",250000, 25)

e1.get_info()
e2.get_info()
e3.get_info()