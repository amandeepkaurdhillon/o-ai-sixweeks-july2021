class Student:
    clz = "ABCD College of Engineering"
    def __init__(self,name="James", rno=1):
        self.n = name 
        self.r = rno 
    def intro(self):
        print("Name: {} Roll Number: {}".format(self.n, self.r))

#Simple Inheritance
class Account(Student):
    branch = "CSE"
    def fee_details(self, total, pending):
        print("Here are Fee Details of: {}".format(self.n))
        print("Total fee: Rs.{}/-".format(total))
        print("Pending fee: Rs.{}/-".format(pending))

s1 = Account()
# print(dir(s1))
print(s1.branch)
print(s1.clz)
s1.intro()  
s1.fee_details(100000,1000)


obj = Student()
print(obj.clz)
# print(obj.branch) ##Parent class' object can;t access child class' attributes