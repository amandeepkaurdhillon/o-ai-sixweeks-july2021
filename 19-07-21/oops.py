class Student:
    #data member
    clz = "ABCD Engg College"
    #member function
    def intro(self,name, rno):
        print("{} with roll no {} registered!".format(name,
        rno))

#Create an object
obj = Student()
print("before: ",obj.clz)
#edit attributes
obj.clz = "XYZ college of management"
print("after: ",obj.clz)
obj.intro("Peter Parker",1001)