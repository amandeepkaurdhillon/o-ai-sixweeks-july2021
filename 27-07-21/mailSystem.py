import tkinter as tk
from tkinter import messagebox
from tkinter import ttk
import os
class MailSystem:
    def __init__(self):
        self.win1=tk.Tk()
        self.win1.title("MAIL SYSTEM")
        self.win1.geometry("500x400")
        self.win1.config(bg="#ad9d55")
        self.win1.resizable(0,0)

        self.b1=tk.Button(self.win1,text="Login",bg="#a66741",pady=8,padx=20,command=self.login)
        self.b2=tk.Button(self.win1,text="Register",bg="#a66741",pady=8,padx=14,command=self.register)

        self.b1.place(x=215,y=100)
        self.b2.place(x=215,y=200)
        self.win1.mainloop()

    def login(self):
        pass
    def register(self):
        self.win4=tk.Tk()
        self.win4.title("REGISTER")
        self.win4.geometry("500x400")
        self.win4.config(bg="#ad9d55")
        self.win4.resizable(0,0)
        global c3
        self.c3=tk.StringVar()
        self.c4=tk.StringVar()
        self.c5=tk.StringVar()

        self.b21=tk.Label(self.win4,text="Name: ",bg="#ad9d55",font=("cursive",15))
        self.b22=tk.Label(self.win4,text="Contact Number: ",bg="#ad9d55",font=("cursive",15))
        self.b23=tk.Label(self.win4,text="Password: ",bg="#ad9d55",font=("cursive",15))
        self.b24=tk.Entry(self.win4,textvariable=self.c3)
        self.b25=tk.Entry(self.win4,textvariable=self.c4)
        self.b26=tk.Entry(self.win4,textvariable=self.c5)

        self.b27=tk.Button(self.win4,text="Register",bg="#a66741",pady=8,padx=20,command=self.registration)

        self.b21.place(x=120,y=110)
        self.b22.place(x=120,y=150)
        self.b23.place(x=120,y=190)
        self.b24.place(x=280,y=115)
        self.b25.place(x=280,y=155)
        self.b26.place(x=280,y=195)
        self.b27.place(x=210,y=270)
        self.win4.mainloop()
    
    def registration(self):
        v3=self.b24.get()
        v4=self.b25.get()
        v5=self.b26.get()
        print(v3,v4, v5)
        if v4.isdigit()==True:
            if len(v4)==10:
                if "Users" not in os.listdir():
                    os.mkdir("Users")
                if v4 not in os.listdir("Users/"):
                    os.mkdir("Users/{}".format(v4))
                else:
                    messagebox.showerror("Format Error","A User with this contact number already exists!")

                path="Users/{}/Profiles.csv".format(v4)
                f=open(path,'x')
                st1=""
                st1+="{},".format(v3)
                st1+="{},".format(v4)
                st1+="{},".format(v5)
                f.write(st1)
                f.close()
                b28=tk.Label(self.win4,text="{} registered successfully".format(v4),bg="#ad9d55",font=("cursive",15))
                b28.place(x=200,y=350)
            else:
                messagebox.showerror("Error","Not a valid Contact Number")
        else:
            messagebox.showerror("Format Error","Contact Number can only be in digits")

obj = MailSystem()