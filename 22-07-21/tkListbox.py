import tkinter as tk 
from tkinter import messagebox
class MyWindow:
    def __init__(self):
        self.win = tk.Tk()
        self.win.title("Tkinter ListBox")
        self.label = tk.Label(self.win, text="CREATE A LIST OF FRUITS",fg="white", bg="orangered",font=("Cursive",20)).pack(ipady=20)
        self.fruit = tk.Entry(self.win)
        self.fruit.pack(pady=10)
        
        self.btn = tk.Button(self.win, text="Add Fruit",bg="green",fg="white",
        command=self.add_fruit)
        self.btn.pack(pady=10)
        
        self.vbtn = tk.Button(self.win, text="View",bg="orange",fg="white",
        command=self.view_fruit)
        self.vbtn.pack(pady=10)
        
        self.delbtn = tk.Button(self.win, text="Delete Fruit",bg="red",fg="white",
        command=self.delete_fruit)
        self.delbtn.pack(pady=10)

        self.fruits = tk.Listbox(self.win)
        self.fruits.insert(1, "Apple")
        self.fruits.insert(2, "Orange")
        self.fruits.pack(pady=10)

    def add_fruit(self):
        f_to_add = self.fruit.get()
        index = self.fruits.size()
        print(index)
        self.fruits.insert(index, f_to_add)

    def view_fruit(self):
        val = self.fruits.curselection()
        sel = self.fruits.get(val[0])
        messagebox.showinfo("Selected", "Selected Fruit: {}".format(sel))

    def delete_fruit(self):
        val = self.fruits.curselection()
        self.fruits.delete(val)
        messagebox.showinfo("Selected", "{} Fruit deleted!".format(val))

if __name__=="__main__":
    obj = MyWindow()
    obj.win.mainloop()