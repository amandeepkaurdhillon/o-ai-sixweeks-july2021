import tkinter as tk

main = tk.Tk()
main.geometry("300x500")
main.title("Tkinter Widgets")
def abcd():
    val = r.get()
    print(val)
def reset():
    c1.toggle()
    c2.toggle()
    c3.toggle()
    # c3.deselect()
    # c3.select()

# personal = tk.LabelFrame(main, text="Personal Information",bg="white")
personal = tk.Frame(main,bg="#96247e")
personal.pack(pady=10, padx=50, ipadx=10, ipady=10)

lbfn = tk.Label(personal, text="First Name:",bg="#96247e",fg="#ffffff")
lbls = tk.Label(personal, text="Last Name:",bg="#96247e",fg="#ffffff")
fn = tk.Entry(personal)
ln = tk.Entry(personal)

lbfn.grid(row=0,column=0,pady=10)
fn.grid(row=0,column=1,pady=10)
lbls.grid(row=1,column=0,pady=10)
ln.grid(row=1,column=1,pady=10)

hobbies = tk.LabelFrame(main,text="hobbies",bg="orange", fg="white")
hobbies.pack(pady=10, padx=50, ipadx=10, ipady=20)

r = tk.IntVar()

c1 = tk.Checkbutton(hobbies, text="Reading Books", onvalue=1, offvalue=0,bg="orange",variable=r,command=abcd)
c2 = tk.Checkbutton(hobbies, text="Travelling", onvalue=2, offvalue=0,bg="orange")
c3 = tk.Checkbutton(hobbies, text="Exploring", onvalue=3, offvalue=0,bg="orange")

c1.grid(row=0,column=0)
c2.grid(row=0,column=1)
c3.grid(row=1,column=0)

buttons = tk.Frame(main)
buttons.pack()

reset = tk.Button(buttons, text="RESET", fg="white", bg="green", relief="flat", command=reset)
reset.config(width=30)
reset.grid(row=0, column=0, columnspan=2)

main.mainloop()