import tkinter as tk
from PIL import Image, ImageTk

main = tk.Tk()
main.title("Display an Image")

path  = "C:/Users/John/Pictures/GetImage.png"
img = Image.open(path)
image = ImageTk.PhotoImage(img)
lbl = tk.Label(main, image=image)
lbl.image = image
lbl.pack()

main.mainloop()