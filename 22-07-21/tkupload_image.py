import tkinter as tk 
from tkinter import filedialog
from PIL import Image, ImageTk
import shutil

class MyWindow:
    def __init__(self):
        self.win = tk.Tk()
        self.win.title("Tkinter Upload Image")
        self.win.minsize(650,500)
        self.lf = tk.LabelFrame(self.win, text="Open A File")
        self.lf.grid(column=0, row=1, padx=20,pady=30,ipadx=50, ipady=50)

        self.button = tk.Button(self.lf, text="Browse a file", command=self.UploadFile)
        self.button.grid(row=1,column=1)

    def UploadFile(self):
        self.file_name = filedialog.askopenfilename(initialdir='/', title="Select A File", filetype=(("jpeg","*.jpg"),("png","*.png")))
        self.label = tk.Label(self.win, text="")
        self.label.grid(row=2, column=1)
        # self.label.configure(text=self.file_name)
        self.display_image()
    
    def display_image(self):
        img = Image.open(self.file_name)
        image = ImageTk.PhotoImage(img)
        lbl = tk.Label(self.win, image=image)
        lbl.image = image
        #Move file from one folder to another
        # shutil.move(self.file_name, "uploads")
        shutil.copy(self.file_name, "uploads")
        lbl.grid(row=3,column=0)

if __name__=="__main__":
    obj = MyWindow()
    obj.win.mainloop()