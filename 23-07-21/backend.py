import tkinter as tk 

def new_window():
    root = tk.Tk()
    root.title("New Window")
    root.minsize(400,400)
    label = tk.Label(root, text="WELCOME TO NEW WINDOW", fg="white", bg="orangered")
    label.config(font=("Arial",24))
    label.pack(ipady=20, fil="x")
    root.mainloop()
    
