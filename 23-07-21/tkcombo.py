import tkinter as tk 
from tkinter import ttk
from tkinter import messagebox
import requests

root = tk.Tk()
root.title("Tkinter Combobox")
root.minsize(300,200)

API = "https://restcountries.eu/rest/v2/all"
data = requests.get(API).json()
names = [con["name"] for con in data]

def show_details(event):
    val = con.get()
    rs = ""
    for i in data:
        if i["name"]==val:
            rs+="Name: {}\n".format(i["name"])
            rs+="Capital: {}\n".format(i["capital"])
            rs+="Population: {}\n".format(i["population"])
            rs+="Area: {}\n".format(i["area"])
            rs+="Borders: {}\n".format(i["borders"])
            break
    result.config(text=rs)
    # messagebox.showinfo("Information", "Selected Value:{}".format(val))

con = tk.StringVar()

countries = ttk.Combobox(root, textvariable=con)
countries.pack(pady=10, fill="x", ipady=8, padx=50)

# countries["values"] = ["India","China","US"]
countries["values"] = names
result = tk.Label(root, text="",fg="green",bg="white",font=("cursive",15))
result.pack()

countries.bind('<<ComboboxSelected>>', show_details)

root.mainloop()