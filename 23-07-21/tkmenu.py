import tkinter as tk 
from backend import *

main = tk.Tk()
main.title("My Options")
main.geometry("500x500")

def open_label():
    lbl.config(text="WELCOME TO THIS WINDOW",fg="white",bg="green",
    font=("courier new",20))

mymenu = tk.Menu(main)
file = tk.Menu(mymenu, tearoff=0)
file.add_command(label="New", command=new_window)
file.add_command(label="Open...", command=open_label)
file.add_command(label="Save")
file.add_separator()
file.add_command(label="Exit", command=main.destroy)
edit = tk.Menu(mymenu, tearoff=0)
edit.add_command(label="Undo")
edit.add_separator()
edit.add_command(label="Cut")
edit.add_command(label="Copy")
mymenu.add_cascade(label="File", menu=file)
mymenu.add_cascade(label="Edit", menu=edit)

lbl = tk.Label(main)
lbl.pack( padx=20,ipady=20)

main.config(menu=mymenu)
main.mainloop()