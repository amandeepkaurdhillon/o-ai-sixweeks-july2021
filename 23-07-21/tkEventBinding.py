import tkinter as tk
from tkinter import font
from tkinter import messagebox

main = tk.Tk()
main.geometry("500x500")
main.title("Tkinter Events")
def rclick(event):
    messagebox.showinfo("Information", "Right Button Clicked!")
def lclick(event):
    messagebox.showinfo("Information", "Left Button Clicked!")
def changeBg(event):
    main.config(bg="black")
def changeBgW(event):
    main.config(bg="white")
def addText(event):
    label.config(text="Mouse Hovered")
def getTable(event=None):
    num = en.get()
    if num.isdigit():
        num=int(num)
        txt = ""
        for i in range(1,11):
            txt+="{} x {} = {}\n".format(num,i,num*i)
        result.config(text=txt)
    else:
        messagebox.showerror("Invalid Value","Please Enter Numbers only!")
en = tk.Entry(main)
en.pack(pady=20,fill="x",  ipady=20,padx=50)

btn = tk.Button(main, text="CLICK HERE", bg="green", 
fg="white",command=getTable)
btn.pack(pady=20,fill="x",  ipady=20,padx=50)

label = tk.Label(main, text="",fg="red",bg="white",font=("cursive",20))
label.pack()

result = tk.Label(main, text="",fg="green",bg="white",font=("cursive",10))
result.pack()

## Event Binding 
# btn.bind("<Button-1>", lclick)
# btn.bind("<Button-3>", rclick)
btn.bind("<Double-3>", changeBg)
btn.bind("<Double-1>", changeBgW)
btn.bind("<Enter>", addText)
btn.bind("<Return>", getTable)

main.mainloop()