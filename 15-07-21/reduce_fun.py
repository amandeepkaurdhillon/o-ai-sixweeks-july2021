from functools import reduce

def add(x,y):
    return x+y

marks = [10,3,5,6,7]

# rs = reduce(add, ls)
rs = reduce(lambda x,y:x+y, marks)
# print(rs/len(marks))
# print('hello there')

student = {
    "name":"James", "branch":"CSE"
}