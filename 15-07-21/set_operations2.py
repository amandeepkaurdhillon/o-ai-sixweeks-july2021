a = {10,20,4,3}
b = {3,33,56,10}

# u = a.union(b)
u = a|b
print(u)

# i = a.intersection(b)
i = a&b
print(i)

# d = a.difference(b)
d =b-a
print(d)

# sd = a.symmetric_difference(b)
sd = a^b
print(sd)
