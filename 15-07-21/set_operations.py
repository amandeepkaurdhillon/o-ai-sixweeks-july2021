st = set()

## Add elements
st.add(10)
st.add(3)
st.add(3)
st.add(30)

# print(type(st))
## Remove elements
# st.pop()
print(st)

ls = list(st)
ls.remove(30)
print(set(ls))

roll_no = [1,3,4,4,4,4,3,3,3,10,20,10]
final = list(set(roll_no))
print(final)