def employee(name, age, salary):
    print('Name:{} Age:{} Salary:{} '.format(name, age, salary))

#Positional Arguments
employee("Peter Parker", 22, 10000)

#Keyword Arguments
employee(salary=20000, name="Harry", age=21)
employee("Jenny", age=22, salary=20000)
# employee("Jay", 23, age=35) ## return error for duplicate values of age

