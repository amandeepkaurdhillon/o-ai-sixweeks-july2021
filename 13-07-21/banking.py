import random
options = '''
    Press 1: To create account
    Press 2: To check balance 
    Press 3: To deposit
    Press 4: To withdraw
    Press 0: To exit
'''
print(options)
all_users = []

def create_account():
    name = input("Enter Your Name: ")
    amt = float(input("Enter initial amount: "))
    pin = random.randint(1000,9999)
    u = {'name':name, 'balance':amt, 'pin':pin}
    all_users.append(u)
    print('Dear {}, Your account created successfully! Pin code: {}\n'.format(name,pin))

def check_balance():
    pin_to_search = int(input("Enter Your Pin Code: "))
    match=False
    for user in all_users:
        if user['pin'] == pin_to_search:
            print("Welcome {}!".format(user['name']))
            print("Here is your current balance: Rs.{}/-\n".format(user['balance']))
            match=True 
    
    if match==False:
        print("Sorry, You are not registered with us!\n")

def deposit():
    pin_to_search = int(input("Enter Your Pin Code: "))
    match=False
    for user in all_users:
        if user['pin'] == pin_to_search:
            print("Welcome {}!".format(user['name']))
            amount = float(input("Enter Amount to deposit: "))
            user['balance'] += amount
            print('Your account credited with Rs.{}/-, Current Balance: Rs.{}/-'.
            format(amount, user['balance']))
            match=True 
    
    if match==False:
        print("Sorry, You are not registered with us!\n")
    
def withdraw():
    pin_to_search = int(input("Enter Your Pin Code: "))
    match=False
    for user in all_users:
        if user['pin'] == pin_to_search:
            print("Welcome {}!".format(user['name']))
            amount = float(input("Enter Amount to deposit: "))
            if amount > user['balance']:
                print("You don't have sufficient balance to make this transaction!")
            else: 
                user['balance'] -= amount
                print('Your account debited with Rs.{}/-, Current Balance: Rs.{}/-'.
                format(amount, user['balance']))
            match=True 
    
    if match==False:
        print("Sorry, You are not registered with us!\n")
while True:
    ch = input("Enter Your Choice: ")
    if ch=="1":
        create_account()
    elif ch=="2":
        check_balance()
    elif ch=="3":
        deposit() 
    elif ch=="4":
        withdraw()
    elif ch=="0":
        break
    else:
        print("Invalid Choice!\n")