# def is_even(x):
#     return x%2==0

# print(is_even(22))
# print(is_even(23))

# #Lambda
# evn = lambda a:a%2==0
# print(evn(33))
# print(evn(30))

# ## Write a lambda function to add two numbers
# add = lambda x,y:x+y
# print(add(23,5))
# print(add(23,34))

# def check_evn(a):
#     if a%2==0:
#         return "{} is even!".format(a)
#     else:
#         return "{} is odd!".format(a)

# print(check_evn(44))
# print(check_evn(33))


# check = lambda a:"{} is even!".format(a) if a%2==0 else "{} is odd!".format(a)
# print(check(34)) 
# print(check(3)) 

ch = lambda x:True if x%2==0 else None
print(ch(8))
print(ch(1))