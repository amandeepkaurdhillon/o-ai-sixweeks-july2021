ls = []
for i in range(1,100):
    if i%8==0:
        ls.append(i)

# print(ls)

# a = [i for i in range(1,100)]
# a = ["Python" for i in range(10)]
a = [i for i in range(1,100) if i%8==0]
print(a)

st = "a*&^Y*()m*&^876543a&^%$&^%$323456n**&^%$#"
# b = [i for i in st if i.isalpha()]
b = [i for i in st if i.isupper()]
print(b)