#WAP to transpose a given matrix with and without using list comprehension

m = [
    [10,2,4],
    [4,5,7],
    [45,6,22]
]

# After transpose
# [
#     [10,4,45],
#     [2,5,6],
#     [4,7,22],
# ]