marks = [12,65,4,34,99,76,124,54,45,80]
# ls = []
# for i in marks:
#     if i>=33:
#         ls.append(i)
# print(ls)

# Syntax :
#     filter(function, sequence)

def check(n):
    return n>=33

result = list(filter(check, marks))
fail = list(filter(lambda x:x<33, marks))
print(fail)

st ="Pgfdre567*&^%$%EYjhgfdertyhj&^T876rfgh*&^%^H986O*&^&%N7865tyg"

# uppers = list(filter(lambda s:s.isupper(), st))
nonalnum = list(filter(lambda s:s.isalnum()==False, st))
print(nonalnum)