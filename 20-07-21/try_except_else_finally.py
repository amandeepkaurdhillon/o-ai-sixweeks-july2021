try:
    a = "Hello world"
    print(a)
    # print(b)

except:
    print("Except Block! Error occurred!")

else:
    print("Else, Yeah! There is no error in try block")

finally:
    print("Finally! I will execute always!")