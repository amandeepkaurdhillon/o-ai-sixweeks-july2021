class Student:
    __pin_code = 11029837654 #Private D.M.
    a = 100 #publich D.M.
    def demo(self): #pubic M.F.
        print("It is a demo member function: ", self.__pin_code)
class Library(Student):
    def __init__(self):
        print("a=", self.a)
        self.demo()
ob = Student()
print(ob.a)
ob.demo()
# print(ob.__pin_code) ## Private DM can't be used outside of a class

ob1 = Library()