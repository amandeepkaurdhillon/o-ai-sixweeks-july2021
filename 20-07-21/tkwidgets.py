import tkinter as tk 
main = tk.Tk()
main.title("Tkinter Widgets")
main.geometry("500x200")
main.resizable(0,1)

#Add label widget to tkinter window
lb1 = tk.Label(main, text="WELCOME TO MY SITE", bg="yellow",
fg="red", font=("Arial", 20))

#geometry manager
lb1.pack(fill="both")

#Add label widget to tkinter window
lb2 = tk.Label(main, text="This is my first widget", bg="#00ff00",
fg="red", font=("Arial", 20))

#geometry manager
lb2.pack(fill="both", pady=20,ipady=50, ipadx=200, padx=50)

main.mainloop()