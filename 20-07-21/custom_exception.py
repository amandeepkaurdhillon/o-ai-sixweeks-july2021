class MyError(Exception):
    def __init__(self, err="First Operand must be greater"):
        self.e = err 
    def __str__(self):
        return self.e

try:
    a = int(input("Enter First Number: "))
    b = int(input("Enter Second Number: "))
    if a<b:
        raise MyError 
    else:
        print("Subtraction: ", a-b)
except MyError as e:
    print("Something Went wrong!")
    print(e)