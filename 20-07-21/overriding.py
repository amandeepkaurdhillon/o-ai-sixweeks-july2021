class Parent:
    salary = 10000
    def __init__(self):
        print("Parent's constructor")

class Child(Parent):
    salary = 15000
    def __init__(self):
        print("salary=", super().salary)
        super().__init__()
        print("Child's constructor")

obj = Child()