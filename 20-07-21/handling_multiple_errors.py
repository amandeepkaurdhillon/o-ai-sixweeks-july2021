try:
    a = 10
    b = ['red','green','blue']
    c = {'name':'aman','rno':1}
    # print(b[4])
    print(c['email'])
    # print(a/0)

except (NameError,IndexError, KeyError):
    print("Please Check Variable Names carefully!")

except ZeroDivisionError:
    print("Number can't be divided by zero")
