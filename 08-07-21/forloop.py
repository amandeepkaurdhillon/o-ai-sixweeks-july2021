# for(i=1; i<10; i++) #Python doesn't support this type of loop
a = "python"
a = ["Python Programming","PHP","HTML","MySQL","React Native"]

# c = 1
# for i in a:
#     print(c,':',i)
#     c = c+1

# for x in range(11):
#     print(x)

# for i in range(1,11,1):
#     print(i)

# for i in range(1,100,2):
#     print(i, end="\t")

# for i in range(0,100,10):
#     print(i, end="\t")

## WAP to print 10 to 1
for i in range(10,0,-1):
    print(i)
