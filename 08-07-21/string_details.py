a = "Python is easy"
print(a)

#Indexing
print(a[0])
print(a[1])
print(a[-1])
print(a[-2])

#Slicing 
# sub = a[2:]
# sub = a[:2]
# sub = a[3:9]
sub = a[-3:]
print(sub)

#Stepping 
b = "Hello World"
b = "0123456789"
print(b[::1])
print(b[::2])
print(b[::3])
print(b[::-1])