colors = ['red','green','blue','magenta',
'orange','pink','black','cyan','yellow']

print("Total Elements: ",len(colors))

index = 0
while index<len(colors):
    # print('index:',index,', value: ',colors[index])
    print('index: {}, value: {}'.format(index, colors[index]))
    index+=1

# index: 0, value: red 
# index: 1, value: green