a = "Python was created by Guido van rossum"

print(a)
print(a.upper())
print(a.lower())
print(a.title())
print(a.capitalize())
print(a.swapcase())

print("hello".isupper())
print("hello".islower())
print("hello".isdigit())
print("hello".isdigit())
print("hello9876^&".isalnum())
print("hello".isalpha())
print("aman".index('a'))
print("aman".index('a',1))
print("hello how are you".title())

colors = "red green blue magenta"
print(colors)
print(colors[0])
s = colors.split(" ")
print(s)
print(s[0])

# TASK
# Enter Your Email ID: amandeep.kaur@sachtechsolution.com 
# Username: amandeep.kaur 
# Domain: sachtechsolution.com 

# ID: aman@yahoo.com
# Username: aman
# Domain: yahoo.com
