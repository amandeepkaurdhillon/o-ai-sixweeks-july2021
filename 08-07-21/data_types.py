a = 10
b = 10.5
c = 1+5j
d = "10"
e = True
f = False
g = ['red','green','orange',10] #Mutable: We can edit update or delete list elements
h = (10,20,30) #immutable : We can't alter its elements
i = {'name':'Peter','age':20}
j = {10,20,30}
k = None 

print(a)
print(type(a))
print(type(b))
print(type(c))
print(type(d))
print(type(e))
print(type(f))
print(type(g))
print(type(h))
print(type(i))
print(type(j))
print(type(k))