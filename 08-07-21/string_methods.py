a = "Python "
b = "is easy language"
c = 10.5

#Concatenation
print(a+b)
# print(a+str(c))

#Iteration
print(a*4)

#Membership
print('Easy' in b)

names = "James Peter Schinchan Harry"
# n = input("Enter Name: ")

# if n in names:
#     print("Student Found!")
# else:
#     print("Student not found!")

print(dir('a'))