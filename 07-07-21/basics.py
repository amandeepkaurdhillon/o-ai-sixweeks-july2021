a = "Hello There"

#To print something use print()
print(a)

#Swapping 
x = 10
y = 20

print("X=",x," Y=",y)
x,y = y,x
print("X=   ",x," Y=",y)