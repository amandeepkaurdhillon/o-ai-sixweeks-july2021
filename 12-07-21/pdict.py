student = {
    'name':'Peter Payen',
    'age':22,
    'is_present':True,
    'hobbies':['Gaming','Reading Books','Travelling'],
    'tech':{
        'front-end':['OpenCV','Tkinter'],
        'back-end':['Python','Sklearn','SQL'],
    }
}

# print(student)
# print(type(student))
# print(len(student))

## Access Elements
print(student['name'])
print("{} is {} years old".format(student['name'], student['age']))
print(student['hobbies'][1])
print(student['tech']['back-end'][1])