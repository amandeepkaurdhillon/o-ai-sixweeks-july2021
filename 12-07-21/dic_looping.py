employee = {
    'name':'James Parker',
    'age':25,
    'salary':40000,
    'experience':'4+',
    'profile':'Software Developer'
}

# for i in employee:
#     print("{} => {}".format(i, employee[i]))


for key, value in employee.items():
    print("{} => {}".format(key, value))