op = """Press 1: To Add element
Press 2: To view elements
Press 3: To remove elements
Press 4: To sum
Press 5: To multiply
Press 0: To exit 
"""

print(op)
all = []
while True:
    ch = input("Enter Your Choice: ")
    if ch=="1":
        n = input("Enter a number: ")
        if n.isdigit():
            all.append(int(n))
            print("{} added successfully!\n".format(n))
        else:
            print("Please enter numbers only!\n")
    elif ch=="2":
        print("Total: {}".format(len(all)))
        c=0
        for i in all:
            c+=1
            print("{} : {}".format(c,i))
    elif ch=="3":
        n = input("Enter number to remove: ")
        if n.isdigit()==False:
            print("Please enter numbers only\n")
        else:
            n = int(n)
            if n in all:
                all.remove(n)
                print("{} removed successfully\n".format(n))
            else:
                print("{} not found in list!\n".format(n))
    elif ch=="4":
        s = 0
        c = 0
        for x in all:
            s+=x
            c+=1
        print("Sum of {} numbers is: {}\n".format(c, s))

    elif ch=="5":
        m = 1
        count = 0
        for a in all:
            m *= a 
            count+=1
        print("Multiplication of {} numbers is: {}\n".format(count, m))

    elif ch=="0":
        break
    else:
        print("Invalid Choice!")