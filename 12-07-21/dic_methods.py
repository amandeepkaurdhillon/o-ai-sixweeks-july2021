employee = {
    'name':'James Parker',
    'age':25,
    'salary':40000,
    'experience':'4+',
    'profile':'Software Developer',
    'hobbies':['Gaming','Reading Books','Travelling'],
}

#Access Value
# print(employee['name'])
# print(employee.get('DOJ'))

## Insert or Update Value
# employee['DOJ'] = 'Jan 2019'
# employee['age'] = 27
# employee.update({'age':26, 'DOJ':'Jan 2018'})
# employee['hobbies'][1]='Exploring'
# employee['hobbies'].append('Music')

##Delete Elements
# print(employee.pop('hobbies'),"removed!")
# employee.popitem()
# employee.popitem()
# c = employee.copy()
# employee.clear()
# print(employee)
# print(c)
# for key, value in employee.items():
#     print("{} => {}".format(key, value))
# print(dir(employee))
# print(employee.items())
# print(employee.keys())
# print(employee.values())

movie = ['name','leading actor', 'leading acteress','revenue', 'released on']
print(movie)
print({}.fromkeys(movie,0))