from tkinter import *
import tkinter as tk
from tkinter import ttk
import requests
import json


states = []

r=requests.get("https://api.covid19india.org/data.json").json()
data = requests.get("https://api.covid19india.org/state_district_wise.json").json()

for i in range(1,38):
            state = r["statewise"][i]["state"]
            states.append(state)

def search(event):
    districts=[]
    state = combo.get()
    for dist in data[state]["districtData"]:
        districts.append(dist)
        
    combo1["values"] = districts
    return 

def search1():
    Listbox1.delete(0, END)
    state = combo.get()
    district = combo1.get()
    Listbox1.insert(1,"Confirmed : {}".format(str(data[state]["districtData"][district]["confirmed"])))
    Listbox1.insert(2,"Recovered : {}".format(str(data[state]["districtData"][district]["recovered"])))
    Listbox1.insert(3,"Deaths : {}".format(str(data[state]["districtData"][district]["deceased"])))
    Listbox1.insert(4,"Active : {}".format(str(data[state]["districtData"][district]["active"])))
    Listbox1.insert(5,"Notes : {}".format(str(data[state]["districtData"][district]["notes"])))
 

top = tk.Tk()
top.resizable(False,False)
top.geometry("600x450+605+100")

top.title("New Toplevel")
top.configure(background="#404040")

Label1 = tk.Label(top)
Label1.place(relx=0.033, rely=0.067, height=31, width=74)
Label1.configure(background="#d9d9d9")
Label1.configure(disabledforeground="#a3a3a3")
Label1.configure(foreground="#000000")
Label1.configure(text='''State''')

Label2 = tk.Label(top)
Label2.place(relx=0.033, rely=0.156, height=31, width=74)
Label2.configure(background="#d9d9d9")
Label2.configure(disabledforeground="#a3a3a3")
Label2.configure(foreground="#000000")
Label2.configure(text='District')

combo = ttk.Combobox(top, values=states)
combo.place(relx=0.200, rely=0.067, relheight=0.047, relwidth=0.238)
# combo.current(15)
combo.bind("<<ComboboxSelected>>", search)

combo1 = ttk.Combobox(top)
combo1.place(relx=0.200, rely=0.156, relheight=0.047 , relwidth=0.238)


Listbox1 = tk.Listbox(top)
Listbox1.place(relx=0.117, rely=0.311, relheight=0.671, relwidth=0.79)
Listbox1.configure(background="white")
Listbox1.configure(disabledforeground="#a3a3a3")
Listbox1.configure(height = 10,  
                  width = 15,  
                  bg = "grey", 
                  activestyle = 'dotbox',  
                  font = "Helvetica", 
                  fg = "yellow")

     

Button1 = tk.Button(top)
Button1.place(relx=0.6, rely=0.089, height=34, width=67)
Button1.configure(activebackground="#ececec")
Button1.configure(activeforeground="#000000")
Button1.configure(background="#d9d9d9")
Button1.configure(command=search1)
Button1.configure(disabledforeground="#a3a3a3")
Button1.configure(foreground="#000000")
Button1.configure(highlightbackground="#d9d9d9")
Button1.configure(highlightcolor="black")
Button1.configure(pady="0")
Button1.configure(text='Search')

top.mainloop()