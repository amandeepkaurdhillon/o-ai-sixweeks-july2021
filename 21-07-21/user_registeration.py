import tkinter as tk
from tkinter import messagebox
import datetime

main=tk.Tk()
main.geometry("340x400")
main.resizable(False, False)
main.title("Login")
main.config(bg="#6b1e42")

def register(un, pwd):
    # try:
    f = open("users.csv","r")
    content = f.read().split("\n")
    found=False 
    for user in content:
        if un==user.split(",")[0]:
            found=True
    if found==False:
        with open("users.csv", "a") as file:
            txt = "{},{},{}\n".format(un, pwd, datetime.datetime.now())
            file.write(txt)
            return True
    else:
        return "error"
    # except:
    #     return False

def get_value():
    username = entry1.get()
    password = entry2.get()
    status = register(username, password)
    if status == True:
        messagebox.showinfo("Success","Registered Successfully!")
    elif status == "error":
        messagebox.showwarning("Success","A user with this username already exists!")
    else:
        messagebox.showerror("Failed","Could not register")

label1 = tk.Label(main, text="Enetr Username: ", bg="#6b1e42", fg="#ffffff")
label2 = tk.Label(main, text="Enetr Password: ", bg="#6b1e42", fg="#ffffff")
entry1 = tk.Entry(main)
entry2 = tk.Entry(main, show="*")
button = tk.Button(main, text="Click To Continue",bg="green", fg="white",
width=17, relief="flat", command=get_value)

#Add Geometry managers
label1.place(x=50, y=50)
entry1.place(x=150, y=50)
label2.place(x=50, y=80)
entry2.place(x=150,y=80)
button.place(x=150,y=120)

main.mainloop()