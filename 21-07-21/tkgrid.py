import tkinter as tk

main=tk.Tk()
main.geometry("340x400")
main.resizable(False, False)
main.title("Login")
main.config(bg="#6b1e42")

label1 = tk.Label(main, text="Enetr Username: ", bg="#6b1e42", fg="#ffffff")
label2 = tk.Label(main, text="Enetr Password: ", bg="#6b1e42", fg="#ffffff")
entry1 = tk.Entry(main)
entry2 = tk.Entry(main, show="*")
button = tk.Button(main, text="Click To Continue",bg="green", fg="white",
width=25, relief="flat")

#Add grid Geometry managers
label1.grid(row=0, column=0, pady=10)
entry1.grid(row=0, column=1, pady=10)
label2.grid(row=1, column=0, pady=10)
entry2.grid(row=1, column=1, pady=10)
button.grid(row=2, column=0, pady=10, columnspan=2)

main.mainloop()