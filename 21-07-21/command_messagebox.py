import tkinter as tk
from tkinter import messagebox

main=tk.Tk()
main.geometry("340x400")
main.resizable(False, False)
main.title("Login")
main.config(bg="#6b1e42")

def get_value():
    username = entry1.get()
    password = entry2.get()
    if username==password:
        messagebox.showinfo("Success","Login Success!")
        dash = tk.Tk()
        dash.title("Dashboard")
        dash.geometry("400x400")
        lbl = tk.Label(dash, font=("arial",24), bg="#6b1e42", fg="white")
        lbl.pack(fill="x", ipady=30, pady=50)
        lbl.config(text="Welcome {}!".format(username))
        dash.mainloop()
    else:
        messagebox.showwarning("Failed","Invalid Login details!")
        # messagebox.showerror("Failed","Invalid Login details!")

label1 = tk.Label(main, text="Enetr Username: ", bg="#6b1e42", fg="#ffffff")
label2 = tk.Label(main, text="Enetr Password: ", bg="#6b1e42", fg="#ffffff")
entry1 = tk.Entry(main)
entry2 = tk.Entry(main, show="*")
button = tk.Button(main, text="Click To Continue",bg="green", fg="white",
width=17, relief="flat", command=get_value)

#Add Geometry managers
label1.place(x=50, y=50)
entry1.place(x=150, y=50)
label2.place(x=50, y=80)
entry2.place(x=150,y=80)
button.place(x=150,y=120)

main.mainloop()